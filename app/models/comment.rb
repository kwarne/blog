class Comment < ActiveRecord::Base
  belongs_to :post
  validates :commentor, :presence => true 
  validates :body, :presence => true 
end
