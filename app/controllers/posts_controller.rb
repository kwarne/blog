class PostsController < ApplicationController
  def index
    @posts = Post.all
  end
  
  def new
    @post = Post.new
  end
  
  def show
    @post = Post.find(params[:id])
  end
  
  def edit
    @post = Post.find(params[:id])
  end
    
  def update
    @post=Post.find(params[:id])
    if @post.update(post_params)
      redirect_to posts_path
    else
      render action: 'edit'
    end
  end
    
    

  
  def create

    @post = Post.new(post_params)
    if @post.save
      redirect_to posts_path #, notice: 'Course was successfully created.' 
    else
      render action: 'new' 
    end

  end
    
    
  def post_params
    params.require(:post).permit(:title, :text)
  end
    
end
