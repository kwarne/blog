class CommentsController < ApplicationController
  def new
    @post = Post.find(params[:post_id])
    @comment = Comment.new
  end

  def edit
    @post = Post.find(params[:post_id])
    @comment = Comment.find(params[:id])
  end

  def update
    @post=Post.find(params[:post_id])
    @comment = Comment.find(params[:id])
    if @comment.update(post_params)
      redirect_to post_path(@post)
    else
      render action: 'edit'
    end
  end
  

  def create
    @post = Post.find(params[:post_id])
    @comment = Comment.new(post_params)
    @post.comments << @comment
    if @comment.save
      redirect_to post_path(@post) #, notice: 'Course was successfully created.' 
    else
      render action: 'new' 
    end
  end

  def destroy
  end
  
  def post_params
    params.require(:comment).permit(:commentor, :body)
  end
  
end
