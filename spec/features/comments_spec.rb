require 'spec_helper'

describe "Comments" do
  before do 
    @post = Post.create :title => 'First Post', :text => 'This is the first of the posts.'
    @comment = Comment.create :commentor => 'David', :body => 'This is a comment for the first post'
    @post.comments << @comment
    
  end
  describe "GET /comments" do
    it "shows a comment under a post" do
      visit post_path(@post)
      
      page.should have_content 'David'
      page.should have_content 'This is a comment for the first post'
      
    end
    
    it "creates a new comment" do
      visit post_path(@post)
      click_on 'New Comment'
      
      
      current_path.should == new_post_comment_path(@post)
      
      fill_in 'comment_commentor', :with => 'Kevin'
      fill_in 'comment_body', :with => 'This is the second comment for the first post'
      
      
      click_on 'Create Comment'
      
      current_path.should == post_path(@post)
      
      page.should have_content 'Kevin'
      page.should have_content 'This is the second comment for the first post'
      
    end
    
    it "edits a comment" do
      
      visit post_path(@post)
      click_on 'Edit'
      
      current_path.should == edit_post_comment_path(@post,@comment)
      
      find_field('Commentor').value.should == 'David'
      find_field('Body').value.should == 'This is a comment for the first post'
      
      fill_in 'comment_commentor', :with => 'David edited'
      fill_in 'comment_body', :with => 'This is a comment for the first post edited'
      
      click_on 'Update Comment'
      
      current_path.should == post_path(@post)
      
      page.should have_content 'David edited'
      page.should have_content 'This is a comment for the first post edited'
      
      
    end
    
    it "validates presence of commentor" do
      visit post_path(@post)
      click_on 'New Comment'
      
      
      current_path.should == new_post_comment_path(@post)
      
      fill_in 'comment_commentor', :with => ''
      fill_in 'comment_body', :with => 'This is the second comment for the first post'
      
      
      click_on 'Create Comment'
      
      current_path.should == post_comments_path(@post)
    end
    it "validates presence of body" do
      visit post_path(@post)
      click_on 'New Comment'
      
      
      current_path.should == new_post_comment_path(@post)
      
      fill_in 'comment_commentor', :with => 'Kevin'
      fill_in 'comment_body', :with => ''
      
      
      click_on 'Create Comment'
      
      current_path.should == post_comments_path(@post)
    end
    
    
  end
  
  describe Comment do
    
    it "belongs to post" do
      should belong_to(:post)
    end
    
  end
  
end
