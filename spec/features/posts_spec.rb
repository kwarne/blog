require 'spec_helper'

describe "Posts" do
  before do 
    @post = Post.create :title => 'First Post', :text => 'This is the first of the posts.'
    @comment = Comment.create :commentor => 'David', :body => 'This is a comment for the first post'
  end
  describe "GET /posts" do
    
    it "displays some posts" do
      
      visit posts_path
      
      page.should have_content 'First Post'
      page.should have_content 'This is the first of the posts.'
      
    end
    
    it "creates a new post" do
      
      visit posts_path
      
      click_on "New Post"
      
      current_path.should == new_post_path
      
      fill_in 'post_title', :with => 'Second Post'
      fill_in 'post_text', :with => 'Here is the second of the posts.'
      
      click_on 'Create Post'

      current_path.should == posts_path
      
      page.should have_content 'Second Post'
      page.should have_content 'Here is the second of the posts.'
      
    end
    
    
    it "shows a post" do
      
      visit posts_path
      
      click_link "Show"
      
      current_path.should == post_path(@post)
      
      page.should have_content 'First Post'
      page.should have_content 'This is the first of the posts.'
      
    end
    
    
    it "edits a post" do
      
      visit posts_path
      
      click_link "Edit"
      
      current_path.should == edit_post_path(@post)
      
      find_field('Title').value.should == 'First Post'
      find_field('Text').value.should == 'This is the first of the posts.'
      
      fill_in 'post_title', :with => 'Second Post edited title'
      fill_in 'post_text', :with => 'Here is the second of the posts. edited text'
      
      click_on 'Update Post'
      
      current_path.should == posts_path
      
      page.should have_content 'Second Post edited title'
      page.should have_content 'Here is the second of the posts. edited text'
      
    end
    
    
    it "validates presence of title" do
      visit posts_path(@post)
      click_on 'New Post'
      
      
      current_path.should == new_post_path
      
      fill_in 'post_title', :with => ''
      fill_in 'post_text', :with => 'This is the first post.'
      
      
      click_on 'Create Post'
      
      current_path.should == posts_path
    end
    it "validates presence of text" do
      visit posts_path(@post)
      click_on 'New Post'
      
      
      current_path.should == new_post_path
      
      fill_in 'post_title', :with => 'First Post'
      fill_in 'post_text', :with => ''
      
      
      click_on 'Create Post'
      
      current_path.should == posts_path
    end
    
    
  end
  
  describe Post do
    
    it "has many comments and is destroy dependent" do
      should have_many(:comments).dependent(:destroy)
    end
    
  end
  
  
  
  
end
